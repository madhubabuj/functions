function getSum(a, b){
    if (Array.isArray(a) || Array.isArray(b)){
      return false;
    }
    if (isNaN(Number(a)) || isNaN(Number(b))){
      return false;
    }
    if (a.length > b.length)
    {
        let t = a;
        a = b;
        b = t;
    }
    let str = "";
    let n1 = a.length, n2 = b.length;
  
    a = a.split("").reverse().join("");
    b = b.split("").reverse().join("");
     
    let carry = 0;
    for(let i = 0; i < n1; i++)
    {
        let sum = ((a[i].charCodeAt(0) -
                        '0'.charCodeAt(0)) +
                   (b[i].charCodeAt(0) -
                        '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
                        '0'.charCodeAt(0));
     
        carry = Math.floor(sum / 10);
    }
     
    for(let i = n1; i < n2; i++)
    {
        let sum = ((b[i].charCodeAt(0) -
                        '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
                        '0'.charCodeAt(0));
        carry = Math.floor(sum / 10);
    }
     
    if (carry > 0)
        str += String.fromCharCode(carry +
                       '0'.charCodeAt(0));
     
    str = str.split("").reverse().join("");
     
    return str;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0;
    let commentsCount = 0;
    for (let post of listOfPosts) {
        if (post.author === authorName) {
            postsCount += 1;
        }
        if (post.comments) {
            for (let comment of post.comments) {
                if (comment.author === authorName) {
                    commentsCount += 1;
                }
            }
        }
    }
    return `Post:${postsCount},comments:${commentsCount}`;
};

function isEmpty(xs) { return xs.length === 0; }
function first(xs) { return xs[0]; }
function rest(xs) { return xs.slice(1); }

function tickets(xs) {
  function loop(a, b, c, xs) {
      if (a < 0 || b < 0 || c < 0)
          return "NO";
      else if (isEmpty(xs))
          return "YES";
      else
          switch (first(xs)) {
              case 25:
                  return loop(a + 1, b, c, rest(xs));
              case 50:
                  return loop(a - 1, b + 1, c, rest(xs));
              case 100:
                  return (b > 0) ? loop(a - 1, b - 1, c + 1, rest(xs)) : loop(a - 3, b, c + 1, rest(xs));
              default: 
                  return "NO";
          }
  }
  return loop(0, 0, 0, xs);
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
